The SuperVP Max externals are based on the same advanced phase vocoder engine as the analysis/synthesis functionalities of [AudioSculpt](https://forum.ircam.fr/projects/detail/audiosculpt/), providing high-quality sound processing in real-time.

A first set of externals gives access to time-stretching, pitch transposition, spectral envelope transformations, and  sound decomposition (i.e. into sinusoids, noise, and transitions). Optionally, the externals allow for preserving transitions and spectral envelopes as well as the exact waveform, and stereo image of the original sound.

Each of the externals provides a specific interface to these functionalities:

- **supervp.trans~**: processes incoming audio (no time-stretching)
- **supervp.ring~**: processes incoming audio within a ring buffer
- **supervp.scrub~**: re-synthesizes sound in a buffer~
- **supervp.play~**: re-synthesizes sound segments from a buffer~

Two further externals of SuperVP for Max are dedicated to cross-synthesis allowing for hybridizing two input audio streams into a single output signal.
The two modules provide different models of cross-synthesis applied to incoming audio streams:

- **supervp.sourcefilter~**: source-filter cross-synthesis
- **supervp.cross~**: generalized cross-synthesis

## Main Applications ##

- Audio Plugins
- Video Games
- Film and Music

> - SuperVP for Max was developped by [**Sound Analysis-Synthesis**](https://www.ircam.fr/recherche/equipes-recherche/anasyn/) Team. 
>
> - **[Voice transformation | multimedia application](http://anasynth.ircam.fr/home/english/media/voice-transformation-multimedia-application)**
> Four characters voices were produced from the one of one actor by transformation of gender and age thanks to Super-VP and VoiceTrans
>
> - **[Processing by Phase Vocoder](https://www.ircam.fr/projects/pages/traitement-par-vocodeur-de-phase/)**